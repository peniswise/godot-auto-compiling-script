# Godot auto-compiling script

# Table of contents
1. Description
2. Dependencies
3. Installation
4. Usage
5. Options
6. Disclaimers

# 1. Description
This is a script that should work on all Godot version releases. What it does is optionally compile both the Godot Engine and the Godot Export Template as well as build a game specified in some path, that was, of course, made by the Godot Engine.

# 2. Dependencies
From https://docs.godotengine.org/en/stable/development/compiling/compiling_for_x11.html

- Alpine Linux

apk add scons pkgconf gcc g++ libx11-dev libxcursor-dev libxinerama-dev libxi-dev libxrandr-dev \
    libexecinfo-dev

- Arch Linux

pacman -S --needed scons pkgconf gcc libxcursor libxinerama libxi libxrandr mesa glu libglvnd \
    alsa-lib pulseaudio yasm

- Debian / Ubuntu

sudo apt-get install build-essential scons pkg-config libx11-dev libxcursor-dev libxinerama-dev \
    libgl1-mesa-dev libglu-dev libasound2-dev libpulse-dev libudev-dev libxi-dev libxrandr-dev yasm

- Fedora

sudo dnf install scons pkgconfig libX11-devel libXcursor-devel libXrandr-devel libXinerama-devel \
    libXi-devel mesa-libGL-devel mesa-libGLU-devel alsa-lib-devel pulseaudio-libs-devel \
    libudev-devel yasm gcc-c++ libstdc++-static

- FreeBSD

sudo pkg install scons pkgconf xorg-libraries libXcursor libXrandr libXi xorgproto libGLU alsa-lib \
    pulseaudio yasm

- Gentoo

emerge -an dev-util/scons x11-libs/libX11 x11-libs/libXcursor x11-libs/libXinerama x11-libs/libXi \
    media-libs/mesa media-libs/glu media-libs/alsa-lib media-sound/pulseaudio dev-lang/yasm

- Mageia

urpmi scons task-c++-devel pkgconfig "pkgconfig(alsa)" "pkgconfig(glu)" "pkgconfig(libpulse)" \
    "pkgconfig(udev)" "pkgconfig(x11)" "pkgconfig(xcursor)" "pkgconfig(xinerama)" "pkgconfig(xi)" \
    "pkgconfig(xrandr)" yasm

- OpenBSD

pkg_add python scons llvm yasm

- openSUSE

sudo zypper install scons pkgconfig libX11-devel libXcursor-devel libXrandr-devel libXinerama-devel \
        libXi-devel Mesa-libGL-devel alsa-devel libpulse-devel libudev-devel libGLU1 yasm

- NetBSD

pkg_add pkg-config py37-scons yasm

For audio support, you can optionally install pulseaudio.

- Solus

sudo eopkg install -c system.devel scons libxcursor-devel libxinerama-devel libxi-devel \
    libxrandr-devel mesalib-devel libglu alsa-lib-devel pulseaudio-devel yasm

See 6. Disclaimers

# 3. Installation:
`curl -sL https://codeberg.org/gitfpsmultiplayergame/godot-auto-compiling-script/raw/branch/main/export-game.sh | sudo tee /usr/bin/export-game >/dev/null && sudo chmod 755 /usr/bin/export-game`

Or you can use `doas` instead of `sudo`.

# 4. Usage
`export-game`

# 5. Options
The table of contents will go from least secure, to most secure, but note that this also means that it takes more time:
1. Answer everything with N - least secure, lots of room for malware and spyware (in general), you trust the game developers and the Godot project with executable files
2. Compile the game yourself - this will compile the game for you, so that you can be 100% certain, that the game code published is the one you'll be running.
    - Will download the Godot project executable file

3. Compile the Godot project yourself - this will compile even the Godot project, so that you have no executable file you need to trust..
The only thing left would be either check for malicious code, or have someone else look at it.. but even without that, this would be more secure than trusting the Godot project who compiled them for you.
    - Will download the Godot project to compile (non-binary form)
You should answer to everything with Y if you don't mind waiting up to a few hours (the last option 3), otherwise, just compiling the game(2) will be sufficient.
- NOTE: Both the option 2 and 3 require downloading the Godot Export Template, which is ~500MB in size!
 
# 6. Disclaimers

Disclaimer 1: This script comes with no warranty

Disclaimer 2: The script has been tested and is working only for GNU+(Arch and Debian)-Linux so far. Please submit your review if you tested this script with a different OS and it worked.. or even if it did not work and you know how to fix it.

Disclaimer 3: You can use this on your own Godot project(s)