#!/bin/bash
# Made by Collider; semi-finished - not archived.
# Git link: https://codeberg.org/gitfpsmultiplayergame/godot-auto-compiling-script
__version__="2021.04.01"
PLATFORM=$(uname -s)
CORES=$(grep "cpu cores" -m 1 /proc/cpuinfo | awk '{print $4}')
HSH="####################"

echo 	"The table of contents will go from least secure, to most secure, but note that this also means that it takes more time:"
echo	"1.   Answer everything with N - least secure, lots of room for malware and spyware (in general), you trust the game developers and the Godot project with executable files"
echo	"2.   Compile the game yourself - this will compile the game for you, so that you can be 100% certain, that the game code published is the one you'll be running."
echo	"	- Will download the Godot project executable file"
echo	"2.5. Compile the Godot project yourself - this will compileeven the Godot project, so that you have no executable file you need to trust.."
echo	"	The only thing left would be either check for malicious code, or have someone else look at it.. but even without that, this would be more secure than trusting the Godot project who compiled them for you."
echo	"	- Will download the Godot project to compile (non-binary form)"
echo	"You should answer to everything with Y if you don't mind waiting up to a few hours (the last option 2.5), otherwise, just compiling the game(2.) will be sufficient."
echo	"NOTE: Both the option 2. and 2.5. require downloading the Godot Export Template, which is ~500MB in size!"
echo 	""

function checkForUpdates()
{
	latestVersion=$(curl -sL "https://codeberg.org/gitfpsmultiplayergame/godot-auto-compiling-script/raw/branch/main/version")
	[[ "$__version__" == "$latestVersion" ]] && echo "I am up to date!"
	[[ "$__version__" != "$latestVersion" ]] && echo "Current version: $__version__" && echo "Latest version: $latestVersion" && echo "Please update me by manually running this command: " && echo "    curl -sL https://codeberg.org/gitfpsmultiplayergame/godot-auto-compiling-script/raw/branch/main/export-game.sh | sudo tee /usr/bin/export-game >/dev/null && sudo chmod 755 /usr/bin/export-game"
}

# Ask if they want to check for updates before starting
read -rp "0/4 Want to check for updates?" yn && yn="$(echo "$yn" | tr '[:upper:]' '[:lower:]')"
[[ "$yn" == y ]] && checkForUpdates
[[ "$yn" != y ]] && [[ "$yn" != n ]] && echo "Please enter a valid option(y/n)." && exit 1

# Ask what the user wants to do
read -rp "1/4 Do you wish to compile the game yourself(Option 2.)?(y/n)" yn && yn="$(echo "$yn" | tr '[:upper:]' '[:lower:]')"
[[ "$yn" == y ]] && COMPILE_GAME=true
[[ "$yn" == n ]] && COMPILE_GAME=false
[[ "$yn" != y ]] && [[ "$yn" != n ]] && echo "Please enter a valid option(y/n)." && exit 1

echo ""

# Exit
[[ "$COMPILE_GAME" == true ]] || (echo "Nothing to do, just start up the executable." && exit 0)

read -rp "2/4 Do you wish to compile Godot yourself(Option 2.5.)?(y/n)" yn && yn="$(echo "$yn" | tr '[:upper:]' '[:lower:]')"
[[ "$yn" == y ]] && COMPILE_GODOT=true
[[ "$yn" == n ]] && COMPILE_GODOT=false
[[ "$yn" != y ]] && [[ "$yn" != n ]] && echo "Please enter a valid option(y/n)." && exit 1

echo ""

# Ask what Godot version do they want to compile shit with
echo "The game developers should specify which Godot version they used to make the game, or you can just figure it out either by correlating the game release date with the Godot releases or just start guessing versions until one of them works :/"
read -er -p "3/4 What version of the Godot engine do you want to compile the game with?(Current stable Godot release is 3.2.3): " -i "3.2.3" GODOT_V

echo ""

# Ask where the game folder is stored at
read -er -p "4/4 Where is the game folder located at? It can be any game made in Godot v$GODOT_V!(Maybe in $HOME/Downloads/Game-Folder ?): " -i "$HOME/" DIR
file -f "$DIR" || (echo "Please check your folder path!" && exit 1)

GODOT="https://github.com/godotengine/godot/archive/$GODOT_V-stable.zip"  
GODOT_X11="https://downloads.tuxfamily.org/godotengine/$GODOT_V/Godot_v$GODOT_V-stable_linux_headless.64.zip"
GODOT_TEMPLATE="https://downloads.tuxfamily.org/godotengine/$GODOT_V/Godot_v$GODOT_V-stable_export_templates.tpz"
GODOT_SERVER_NAME="Godot_v$GODOT_V-stable_linux_headless.64"
GODOT_EXPORT_NAME="Godot_v$GODOT_V-stable_export_templates"

echo ""

[[ -f "/usr/bin/godot" ]] && COMPILED_GODOT_V=$(godot --version | sed 's/[^0-9]*//g')

echo "$HSH Detected $CORES cores on the system! $HSH" && echo ""
case "$PLATFORM" in
	'Linux')
		echo ""
		echo "$HSH Downloads will go to /tmp and will be removed afterwards $HSH"
		# TODO perhaps choose a different directory and don't delete it after we're done? Not sure in which one yet
		echo ""
		cd /tmp || (echo "$HSH ERROR: No such directory! $HSH" && exit 1)
		if [[ "$COMPILE_GODOT" == true ]]; then	
			# Compile both the Godot Engine, the Export Template and out game, for maximum security uwu
			rm "$GODOT_V-stable.zip"
			echo "$HSH Downloading the latest stable version of the Godot project $HSH"
			echo ""
			wget "$GODOT" || (echo "$HSH ERROR: Could not download the Godot project for some reason! Check your internet connection! $HSH" && exit 1)
			echo ""
			echo "$HSH Extracting the project $HSH"
			echo ""
			unzip "$GODOT_V-stable.zip" || (echo "$HSH ERROR: Failed to extract the Godot compressed folder! $HSH" && exit 1)
			echo ""
			echo "$HSH Going to that newly extracted folder $HSH"
			echo ""
			rm "$GODOT_V-stable.zip"
			cd "godot-$GODOT_V-stable" || (echo "$HSH ERROR: No such directory! $HSH" && exit 1)
	
			# Compile shit
			echo "$HSH Compiling the Godot Engine $HSH"
			echo ""
			scons -j"$CORES" platform=server tools=yes target=release_debug || (echo "$HSH ERROR: Compiling the Godot Engine FAILED! $HSH" && exit 1)
			echo ""
			echo "$HSH Compiling the Godot Export Template $HSH"
			echo ""
			scons -j"$CORES" platform=x11 tools=no target=release bits=64 || (echo "$HSH ERROR: Compiling the Godot Export Template FAILED! $HSH" && exit 1)
			echo ""
			echo "$HSH Successfully compiled the Godot project $HSH"
			echo ""

			echo "$HSH Making the Godot Templates directory under ~/.local and copying the Export Template to there, so that the engine can find it $HSH" && echo &&
			mkdir -p "$HOME/.local/share/godot/templates/$GODOT_V.stable/"
			cp "/tmp/godot-$GODOT_V-stable/bin/godot.x11.opt.64" "$HOME/.local/share/godot/templates/$GODOT_V.stable/linux_x11_64_release"
			
			echo "$HSH Exporting the game using the Godot Export Template $HSH"
			echo ""
			# Finally, build the game project using the Godot Engine and the Export Template that we either downloaded or downloaded+compiled.
			cd "$DIR" || (echo "$HSH ERROR: No such directory! $HSH" && exit 1)
			/tmp/godot-"$GODOT_V"-stable/bin/godot_server.x11.opt.tools.64 --export "Linux/X11" "Linux-game-executable"
			echo ""

			echo "$HSH Cleaning up! $HSH"
			rm -r "/tmp/godot-$GODOT_V-stable"
		else
			TEMP_V=$(echo "$GODOT_V" | sed 's/[^0-9]*//g')
			if [[ "$COMPILED_GODOT_V" != "$TEMP_V" ]]; then                                                                  
				rm "$GODOT_SERVER_NAME"
				# Compile the game project with the pre-compiled Godot executable file
				echo "$HSH Downloading the latest stable version of the Godot project executable file $HSH"
				echo ""
				wget "$GODOT_X11" || (echo "$HSH ERROR: Could not download the Godot project for some reason! Check your internet connection! $HSH" && exit 1)
				echo ""
				echo "$HSH Extracting the project $HSH"
				echo ""
				unzip "$GODOT_SERVER_NAME.zip" || (echo "$HSH ERROR: Failed to extract the Godot compressed folder! $HSH" && exit 1)
				echo ""
				echo "$HSH Going to that newly extracted folder $HSH"
				echo ""
				rm "$GODOT_SERVER_NAME.zip"
			fi

			# Loop trough all files in the directory .local/share/godot/templates/ , strip them of all letters and check if we have that version already or not.
			
			DIR_LIST=$(ls "$HOME/.local/share/godot/templates")
			for DIR_ITEM in $DIR_LIST; do  
			        [[ "$COMPILED_GODOT_V" == "$TEMP_V" ]] && HAS_TEMPLATE=true && echo "$HSH We found a pre-downloaded Godot Export Template of the version the user specified $HSH"
			done

			if [[ "$HAS_TEMPLATE" != true ]]; then
				echo "$HSH Downloading the Godot Export Template and extracting it $HSH"
				echo ""
				wget "$GODOT_TEMPLATE" || (echo "$HSH ERROR: Could not download the Godot Export Template for some reason! Check your internet connection! $HSH" && exit 1)
				echo ""
				# Rename the file to .zip since .TZP files are renamed .ZIP files
				mv "$GODOT_EXPORT_NAME.tpz" "$GODOT_EXPORT_NAME.zip"
				unzip "$GODOT_EXPORT_NAME.zip"
				echo ""
				rm "$GODOT_EXPORT_NAME.zip"

				mkdir -p "$HOME/.local/share/godot/templates/$GODOT_V.stable/"
				cp "templates/linux_x11_64_release" "$HOME/.local/share/godot/templates/$GODOT_V.stable/linux_x11_64_release"
				rm -r "templates"
			fi

			# Finally, build the game project using the Godot Engine and the Export Template that we either downloaded or downloaded+compiled.
			echo "$HSH Exporting the game using the Godot Export Template $HSH"
			echo ""

			cd "$DIR" || (echo "$HSH ERROR: No such directory! $HSH" && exit 1)

			if [[ "$HAS_TEMPLATE" == false ]]; then
				/tmp/"$GODOT_SERVER_NAME" --export "Linux/X11" "Linux-game-executable"
			else
				godot --export "Linux/X11" "Linux-game-executable"
			fi
			
			echo ""
			echo "$HSH Cleaning up! $HSH"
			rm "/tmp/$GODOT_SERVER_NAME"
		echo ""
		echo "$HSH It's okay if there are a few errors in the last few lines above, what matters is that there is an executable outputed and you can try it to see if it works or not :) $HSH"
		fi;;
*);;
esac

exit 0